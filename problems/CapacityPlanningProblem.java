package problems;
/**
 * Objects of this class represent instances of the capacity planning problem.
 * @author lct495
 */
public class CapacityPlanningProblem {
    private final int nFacilities;
    private final int nCustomers;
    private final double capacityCosts[];
    private final double deliveryCosts[][];
    private final double demands[];
    private final double maxCapacity[];

    public CapacityPlanningProblem(int nFacilities, int nCustomers, double[] capacityCosts, double[][] deliveryCosts, double[] demands, double[] maxCapacity) {
        this.nFacilities = nFacilities;
        this.nCustomers = nCustomers;
        this.capacityCosts = capacityCosts;
        this.deliveryCosts = deliveryCosts;
        this.demands = demands;
        this.maxCapacity = maxCapacity;
    }

    public int getnFacilities() {
        return nFacilities;
    }

    public int getnCustomers() {
        return nCustomers;
    }

    public double[] getCapacityCosts() {
        return capacityCosts;
    }

    public double[][] getDeliveryCosts() {
        return deliveryCosts;
    }

    public double[] getDemands() {
        return demands;
    }

    public double[] getMaxCapacity() {
        return maxCapacity;
    }
    
    
    
    
}
