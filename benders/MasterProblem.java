/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package benders;

import ilog.concert.IloException;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.concert.IloRange;
import ilog.cplex.IloCplex;
import problems.CapacityPlanningProblem;

/**
 *
 * @author lct495
 */
public class MasterProblem {
    
    private final IloCplex model;
    private final IloNumVar x[];
    private final IloNumVar phi;
    private final CapacityPlanningProblem cpp;

    /**
     * Creates the Master Problem
     * @param cpp
     * @throws IloException 
     */
    public MasterProblem(CapacityPlanningProblem cpp) throws IloException {
        this.cpp = cpp;
        
        // 1. Every model needs an IloCplex object
        this.model = new IloCplex();
        
        // 2. Creates the decision variables
        // we have three plants, thus we create an array
        // of length nFacilities which will contain objects of type IloNumVar
        this.x = new IloNumVar[cpp.getnFacilities()];
        
        // Assigns to each position in the array an IloNumVar object 
        // i.e., a decisio variable in Cplex's language
        for(int i = 1; i<= cpp.getnFacilities(); i++){
            x[i-1] = model.numVar(0, cpp.getMaxCapacity()[i-1],"x"+i);
        }
        this.phi = model.numVar(0, Double.POSITIVE_INFINITY,"phi");
        
        // 3. Creates the objective function
        // Creates an empty linear numerical expression (linear equation)
        IloLinearNumExpr obj = model.linearNumExpr();
        // Adds terms to the equation
        for(int i = 1; i <= cpp.getnFacilities(); i++){
            obj.addTerm(cpp.getCapacityCosts()[i-1], x[i-1]);
        }
        obj.addTerm(1, phi);
        
        // Tells cplex to minimize the objective function
        model.addMinimize(obj);
        
        
    }
    /**
     * Solves the Master Problem.
     * @throws IloException 
     */
    public void solve() throws IloException{
        // Setting the output to null we suppress logs
        // regarding the (dual) simplex iterations
        model.setOut(null);
        
        // Solves the problem
        model.solve();
    }
    /**
     * Returns the X component of the current solution
     * @return the X component of the current solution
     * @throws IloException 
     */
    public double[] getX() throws IloException{
        double X[] = new double[cpp.getnFacilities()];
        for(int i = 1; i<= cpp.getnFacilities() ;i++){
            X[i-1] = model.getValue(x[i-1]);
        }
        return X;
    }
    /**
     * Returns the current value of phi.
     * @return the value of phi.
     * @throws IloException 
     */
    public double getPhi() throws IloException{
        return model.getValue(phi);
    }
    /**
     * Generates and adds a feasibility cut
     * @param dualsCapacityConstraints
     * @param dualsDemandConstraints
     * @throws IloException 
     */
    public void generateFeasibilityCut(double dualsCapacityConstraints[], double dualsDemandConstraints[]) throws IloException{
        // Generates the constant part of the cut
        IloLinearNumExpr lhs = model.linearNumExpr();
        double constant = 0;
        for(int j = 1; j <= cpp.getnCustomers(); j++){
            constant+= cpp.getDemands()[j-1] * dualsDemandConstraints[j-1];
        }
        lhs.setConstant(constant);
        // Generates the term in x
        for(int i = 1; i <= cpp.getnFacilities(); i++){
            lhs.addTerm(dualsCapacityConstraints[i-1], x[i-1]);
        }
        IloRange cut = model.addLe(lhs, 0);
        System.out.println("Added feasibility cut "+cut.toString());
    }
    /**
     * Generates and adds an optimality cut
     * @param dualsCapacityConstraints
     * @param dualsDemandConstraints
     * @throws IloException 
     */
    public void generateOptimalityCut(double dualsCapacityConstraints[], double dualsDemandConstraints[]) throws IloException{
        // Generates the constant part of the cut
        IloLinearNumExpr lhs = model.linearNumExpr();
        double constant = 0;
        for(int j = 1; j <= cpp.getnCustomers(); j++){
            constant += cpp.getDemands()[j-1] * dualsDemandConstraints[j-1];
        }
        lhs.setConstant(constant);
        // Generates the term in x
        for(int i = 1; i <= cpp.getnFacilities(); i++){
            lhs.addTerm(dualsCapacityConstraints[i-1], x[i-1]);
        }
        // Generates the term in phi
        lhs.addTerm(-1,phi);
        IloRange cut = model.addLe(lhs, 0);
        System.out.println("Added optimality cut "+cut.toString());
    }
    /**
     * Returns the objective value
     * @return
     * @throws IloException 
     */
    public double getObjective() throws IloException{
        return model.getObjValue();
    }
    /**
     * Returns the cost of the first stage part of the solution only.
     * @return the cost of the x solution.
     * @throws IloException 
     */
    public double getFirstStageCost() throws IloException{
        double cost = 0;
        for(int i = 1; i <= cpp.getnFacilities(); i++){
            cost += cpp.getCapacityCosts()[i-1] * model.getValue(x[i-1]);
        }
        return cost;
    }
    
    /**
     * Releases all the objects retained by the IloCplex object.
     * In this particular application it makes no difference.
     * However, in bigger and resource-intensive applications
     * it is advised (if not necessary) to release the resources
     * used by the IloCplex object in order for the program to work well.
     * Note that once the method end() has been called, the IloCplex object
     * cannot be used (e.g., queried) anymore.
     */
    public void end(){
        model.end();
    }
    
    public void print(){
        System.out.println(model.toString());
    }
    
}
