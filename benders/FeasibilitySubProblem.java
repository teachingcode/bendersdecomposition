/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package benders;

import ilog.concert.IloException;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.concert.IloRange;
import ilog.cplex.IloCplex;
import problems.CapacityPlanningProblem;

/**
 *
 * @author lct495
 */
public class FeasibilitySubProblem {
    
    private final IloCplex model;
    private final IloNumVar y[][];
    private final IloNumVar v1[];
    private final IloNumVar v2[];
    private final CapacityPlanningProblem cpp;
    private final IloRange capacityConstraints[];
    private final IloRange demandConstraints[];
    /**
     * Creates the LP model for the feasibility subproblem.
     * Particularly, this class represents the dual (the one with slack and surplus variables).
     * @param cpp
     * @param X a solution to MP
     * @throws IloException 
     */
    public FeasibilitySubProblem(CapacityPlanningProblem cpp, double X[]) throws IloException {
        this.cpp = cpp;
        
        // 1. Every model needs an IloCplex object
        this.model = new IloCplex();
        
        // 2. Creates the decision variables
        y = new IloNumVar[cpp.getnFacilities()][cpp.getnCustomers()];
        
        // Assigns to each position in the array an IloNumVar object 
        // i.e., a decision variable in Cplex's language
        for(int i = 1; i<= cpp.getnFacilities(); i++){
            for(int j = 1; j <= cpp.getnCustomers(); j++){
                y[i-1][j-1] = model.numVar(0, Double.POSITIVE_INFINITY);
            }
        }
        // Creates the auxiliary variables
        this.v1 = new IloNumVar[cpp.getnFacilities()];
        for(int i = 1; i<= cpp.getnFacilities(); i++){
            v1[i-1] = model.numVar(0, Double.POSITIVE_INFINITY);
        }
        this.v2 = new IloNumVar[cpp.getnCustomers()];
        for(int i = 1; i<= cpp.getnCustomers(); i++){
            v2[i-1] = model.numVar(0, Double.POSITIVE_INFINITY);
        }
        
        // 3. Creates the objective function
        // Creates an empty linear numerical expression (linear equation)
        IloLinearNumExpr obj = model.linearNumExpr();
        
        // The objective consists of minimizing the sum of the slack and surplus variables. 
        // Adds terms to the equation
        for(int i = 1; i<= cpp.getnFacilities(); i++){
            obj.addTerm(1, v1[i-1]);
        }
        for(int j = 1; j <= cpp.getnCustomers(); j++){
            obj.addTerm(1, v2[j-1]);
        }
        
        // Tells cplex to minimize the objective function
        model.addMinimize(obj);
        
        // 4. Creates the constraints
        // For each constraints creates an populates a linear equation
        
        this.capacityConstraints = new IloRange[cpp.getnFacilities()];
        for(int i = 1; i<= cpp.getnFacilities(); i++){
            IloLinearNumExpr lhs = model.linearNumExpr();
            for(int j = 1; j <= cpp.getnCustomers(); j++){
                lhs.addTerm(1, y[i-1][j-1]);
            }
            // Notice the surplus variable in the capacity constraint
            lhs.addTerm(-1, v1[i-1]);
            capacityConstraints[i-1] = model.addLe(lhs, X[i-1]);
        }
        
        
        this.demandConstraints = new IloRange[cpp.getnCustomers()];
        for(int j = 1; j <= cpp.getnCustomers(); j++){
            IloLinearNumExpr lhs = model.linearNumExpr();
            for(int i = 1; i<= cpp.getnFacilities(); i++){
                lhs.addTerm(1, y[i-1][j-1]);
            }
            // Notice the slack variable in the demand constraint
            lhs.addTerm(1, v2[j-1]);
            demandConstraints[j-1] = model.addGe(lhs, cpp.getDemands()[j-1]);
        }
        
        
    }
    /**
     * Solves the problem.
     * @throws IloException 
     */
    public void solve() throws IloException{
        model.setOut(null);
        model.solve();
    }
    /**
     * Returns the objective value
     * @return the objective value
     * @throws IloException 
     */
    public double getObjective() throws IloException{
        return model.getObjValue();
    }
    /**
     * Returns the array of dual variables for the capacity constraints
     * @return the optimal duals of the capacity constraints
     * @throws IloException 
     */
    public double[] getDualsCapacityConstraints() throws IloException{
        double duals[] = new double[cpp.getnFacilities()];
        for(int i = 1; i <= cpp.getnFacilities(); i++){
            duals[i-1] = model.getDual(capacityConstraints[i-1]);
        }
        return duals;
    }
    /**
     * Returns the array of dual variables for the demand constraints
     * @return the optimal duals of the demand constraints
     * @throws IloException 
     */
    public double[] getDualsDemandConstraints() throws IloException{
        double duals[] = new double[cpp.getnCustomers()];
        for(int i = 1; i <= cpp.getnCustomers(); i++){
            duals[i-1] = model.getDual(demandConstraints[i-1]);
        }
        return duals;
    }
    
    /**
     * Releases all the objects retained by the IloCplex object.
     * In this particular application it makes no difference.
     * However, in bigger and resource-intensive applications
     * it is advised (if not necessary) to release the resources
     * used by the IloCplex object in order for the program to work well.
     * Note that once the method end() has been called, the IloCplex object
     * cannot be used (e.g., queried) anymore.
     */
    public void end(){
        model.end();
    }
    
}
