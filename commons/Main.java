package commons;

import benders.FeasibilitySubProblem;
import benders.MasterProblem;
import benders.OptimalitySubProblem;
import ilog.concert.IloException;
import models.CapacityPlanningModel;
import problems.CapacityPlanningProblem;

/**
 *
 * @author lct495
 */
public class Main {
    
    public static void main(String[] args) throws IloException{
        // Populates the data of the problem
        int nFacilities = 3;
        int nCustomers = 4;
        
        double capacityCosts[] = new double[nFacilities];
        capacityCosts[0] = 10;
        capacityCosts[1] = 14;
        capacityCosts[2] = 13;
        // Populates a matrix of delivery costs
        double deliveryCosts[][] = new double[nFacilities][nCustomers];
        deliveryCosts[0][0] = 10;
        deliveryCosts[0][1] = 10;
        deliveryCosts[0][2] = 10;
        deliveryCosts[0][3] = 10;
        deliveryCosts[1][0] = 9;
        deliveryCosts[1][1] = 8;
        deliveryCosts[1][2] = 7;
        deliveryCosts[1][3] = 11;
        deliveryCosts[2][0] = 11;
        deliveryCosts[2][1] = 6;
        deliveryCosts[2][2] = 5;
        deliveryCosts[2][3] = 10;
        
        // Pupulates the array of demands
        double demands[] = new double[nCustomers];
        demands[0] = 10;
        demands[1] = 19;
        demands[2] = 21;
        demands[3] = 30;
        
        // Populates the array of upper bounds on the capacities
        double maxCapacity[] = new double[nFacilities];
        maxCapacity[0] = 55;
        maxCapacity[1] = 23;
        maxCapacity[2] = 32;
        
        // Creates an instance of the capacity planning problem
        CapacityPlanningProblem cpp = new CapacityPlanningProblem(nFacilities, nCustomers, capacityCosts, deliveryCosts, demands, maxCapacity);
        
        // Creates the Master Problem
        MasterProblem mp = new MasterProblem(cpp);
        
        // Performs the Benders loop until the algorithm converged
        boolean converged = false;
        int iteration = 1;
        while(!converged){
            System.out.println("=====================");
            System.out.println("ITERATION "+iteration);
            System.out.println("=====================");
            mp.print();
            // Solves MP
            mp.solve();
            
            // Gets a lower bound            
            System.out.println("Lower Bound "+mp.getObjective());
            
            // Gets the solution to MP
            double[] X = mp.getX();
            
            // Checks if X is in the projection, that is if the 
            // second stage is feasible, by solving a feasibility subproblem
            FeasibilitySubProblem fsp = new FeasibilitySubProblem(cpp,X);
            fsp.solve();
            
            // Gets the objective value of the feasibility subproblem.
            // If the objective of the fsp is strictly positive
            // we need a feasibility cut (remember Farkas lemma)
            double fspObjective = fsp.getObjective();
            if(fspObjective > 0){
                // In this case we need a feasibility cut.
                
                // First it gets the dual variables
                double[] dualsCapacityConstraints = fsp.getDualsCapacityConstraints();
                double[] dualsDemandConstraints = fsp.getDualsDemandConstraints();
                fsp.end();
                
                // We pass the dual variables to MP to generate a cut
                mp.generateFeasibilityCut(dualsCapacityConstraints, dualsDemandConstraints);
                
            }else{
                // We release the resources assigned to fsp because we 
                // won't use it anymore
                fsp.end();
                
                // We check if we need an optimality cut
                
                // We solve an optimality subproblem
                OptimalitySubProblem osp = new OptimalitySubProblem(cpp,X);
                osp.solve();
                
                // We print an upper bound
                System.out.println("Upper bound = "+(mp.getFirstStageCost() + osp.getObjective()));
                
                // Checks optimality
                if(mp.getPhi() >= osp.getObjective()){
                    
                    // In this case the method converged
                    converged = true;
                    
                    // We print the solution
                    System.out.println("The Benders optimal solution is ");
                    for(int i = 1; i <= nFacilities; i++){
                        System.out.println("X"+i+" = "+X[i-1]);
                    }
                    double[][] Y = osp.getSolution();
                    for(int i = 1; i <= nFacilities; i++){
                        for(int j = 1; j <= nCustomers; j++){
                            System.out.println("Y"+i+","+j+" = "+Y[i-1][j-1]);
                        }
                    }
                    System.out.println("Benders Optimal objective "+mp.getObjective());
                    // Releases the resources
                    osp.end();
                    mp.end();
                }else{
                    // In this case we need to add an optimality cut
                          
                    // First it gets the dual variables
                    double[] dualsCapacityConstraints = osp.getDualsCapacityConstraints();
                    double[] dualsDemandConstraints = osp.getDualsDemandConstraints();
                    
                    
                    // Then adds an optimality cut
                    mp.generateOptimalityCut(dualsCapacityConstraints, dualsDemandConstraints);
                    
                    // Finally, we release the resources
                    osp.end();
                }
            }
            iteration++;
        }
        System.out.println("OPTIMAL SOLUTION WITHOUT DECOMPOSITION");
        // Checks if the results are consistent with solving the full problem
        CapacityPlanningModel m = new CapacityPlanningModel(cpp);
        m.solve();
        
    }
    
}
