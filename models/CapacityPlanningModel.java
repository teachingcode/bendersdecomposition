package models;



import ilog.concert.IloException;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.cplex.IloCplex;
import ilog.cplex.IloCplex.MIPInfoCallback;
import problems.CapacityPlanningProblem;

/**
 * Objects of this class represent models of instances of the 
 * capacity planning problem. 
 * @author lct495
 */
public class CapacityPlanningModel {
    
    private final IloCplex model;
    private final IloNumVar x[];
    private final IloNumVar y[][];
    private final CapacityPlanningProblem cpp;
    /**
     * Creates the LP model for the original problem
     * (without decomposition).
     * @param cpp
     * @throws IloException 
     */
    public CapacityPlanningModel(CapacityPlanningProblem cpp) throws IloException {
        this.cpp = cpp;
        
        // 1. Every model needs an IloCplex object
        this.model = new IloCplex();
        
        // 2. Creates the decision variables
        x = new IloNumVar[cpp.getnFacilities()];
        y = new IloNumVar[cpp.getnFacilities()][cpp.getnCustomers()];
        // Assigns to each position in the array an IloNumVar object 
        // i.e., a decisio variable in Cplex's language
        for(int i = 1; i<= cpp.getnFacilities(); i++){
            x[i-1] = model.numVar(0, cpp.getMaxCapacity()[i-1]);
        }
        for(int i = 1; i<= cpp.getnFacilities(); i++){
            for(int j = 1; j <= cpp.getnCustomers(); j++){
                y[i-1][j-1] = model.numVar(0, Double.POSITIVE_INFINITY);
            }
        }
        
        // 3. Creates the objective function
        // Creates an empty linear numerical expression (linear equation)
        IloLinearNumExpr obj = model.linearNumExpr();
        // Adds terms to the equation
        for(int i = 1; i <= cpp.getnFacilities(); i++){
            obj.addTerm(cpp.getCapacityCosts()[i-1], x[i-1]);
        }
        for(int i = 1; i<= cpp.getnFacilities(); i++){
            for(int j = 1; j <= cpp.getnCustomers(); j++){
                obj.addTerm(cpp.getDeliveryCosts()[i-1][j-1], y[i-1][j-1]);
            }
        }
        
        // Tells cplex to minimize the objective function
        model.addMinimize(obj);
        
        // 4. Creates the constraints
        // For each constraints creates an populates a linear equation
        
        // Capacity constraints
        for(int i = 1; i<= cpp.getnFacilities(); i++){
            IloLinearNumExpr lhs = model.linearNumExpr();
            for(int j = 1; j <= cpp.getnCustomers(); j++){
                lhs.addTerm(1, y[i-1][j-1]);
            }
            lhs.addTerm(-1, x[i-1]);
            model.addLe(lhs, 0);
        }
        
        // Demand constraints
        for(int j = 1; j <= cpp.getnCustomers(); j++){
            IloLinearNumExpr lhs = model.linearNumExpr();
            for(int i = 1; i<= cpp.getnFacilities(); i++){
                lhs.addTerm(1, y[i-1][j-1]);
            }
            model.addGe(lhs, cpp.getDemands()[j-1]);
        }
        
        
    }
    /**
     * Solves the problem and prints the solution. 
     * @throws IloException 
     */
    public void solve() throws IloException{
        model.setOut(null);
        model.solve();
        IloCplex.Status status = model.getStatus();
        System.out.println("STATUS = "+status.toString());
        
        System.out.println("Optimal objective value "+model.getObjValue());
        System.out.println("Optimal solution ");
        for(int i = 1; i<=cpp.getnFacilities() ;i++){
            System.out.println("x_"+i+" = "+model.getValue(x[i-1]));
        }
        for(int i = 1; i<= cpp.getnFacilities(); i++){
            for(int j = 1; j<=cpp.getnCustomers() ;j++){
                System.out.println("y_"+i+","+j+" = "+model.getValue(y[i-1][j-1]));
            }
        }
    }
    
    /**
     * Releases all the objects retained by the IloCplex object.
     * In this particular application it makes no difference.
     * However, in bigger and resource-intensive applications
     * it is advised (if not necessary) to release the resources
     * used by the IloCplex object in order for the program to work well.
     * Note that once the method end() has been called, the IloCplex object
     * cannot be used (e.g., queried) anymore.
     */
    public void end(){
        model.end();
    }
    
}
